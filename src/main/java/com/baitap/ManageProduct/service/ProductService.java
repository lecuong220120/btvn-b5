package com.baitap.ManageProduct.service;

import com.baitap.ManageProduct.enities.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    List<Product> finAllProduct();
    void deleteProduct(Long id);
    Product findProductById(Long id);
    Product saveProduct(Product product);
}
