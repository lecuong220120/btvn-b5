package com.baitap.ManageProduct.service;

import com.baitap.ManageProduct.enities.Product;
import com.baitap.ManageProduct.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService{
    @Autowired
    ProductRepository productRepository;
    @Override
    public List<Product> finAllProduct() {
        List<Product> products = productRepository.findAll();
        return products;
    }

    @Override
    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Product findProductById(Long id) {
        Product product = productRepository.findById(id).get();
        return product;
    }

    @Override
    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }
}
