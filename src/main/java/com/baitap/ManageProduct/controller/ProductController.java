package com.baitap.ManageProduct.controller;

import com.baitap.ManageProduct.enities.Product;
import com.baitap.ManageProduct.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins ="http://localhost:3000")
@RequestMapping("/api")
@RestController
public class ProductController {
    @Autowired
    private ProductService productService;
    @GetMapping("/findAllProduct")
    public ResponseEntity<Product> getAllProduct(){
        List<Product> products = productService.finAllProduct();
        return new ResponseEntity(products, HttpStatus.OK);
    }
    @GetMapping("findProductById/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable Long id){
        Product product = productService.findProductById(id);
        return new ResponseEntity<>(product,HttpStatus.OK);
    }
    @PostMapping("/save")
    public ResponseEntity<Product> saveProduct(@RequestBody Product product){
        return new ResponseEntity<>(productService.saveProduct(product),HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    public void deleteProduct(@PathVariable Long id){
       productService.deleteProduct(id);
    }
    @PutMapping("/update/{id}")
    public  ResponseEntity<Product> updateProduct(@RequestBody Product product,@PathVariable Long id){
        Product product1 = productService.findProductById(id);
        product1.setPrice(product.getPrice());
        product1.setName(product.getName());
        product1.setQuantity(product.getQuantity());
        return new ResponseEntity(productService.saveProduct(product1),HttpStatus.OK);
    }
}
